import imutils
import cv2
import numpy as np

img = cv2.GaussianBlur(cv2.imread('imgs/1.jpg', 1), (11, 11), 0)
mask = cv2.inRange(cv2.cvtColor(img, cv2.COLOR_BGR2HSV), (0, 0, 30), (0, 80, 255))
mask = cv2.erode(mask, None, iterations=2)
mask = cv2.dilate(mask, None, iterations=2)

cnts = imutils.grab_contours(cv2.findContours(mask.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE))

if len(cnts) > 0:
    c = max(cnts, key=cv2.contourArea)
    ((x, y), radius) = cv2.minEnclosingCircle(c)
    M = cv2.moments(c)
    center = (int(M["m10"] / M["m00"]), int(M["m01"] / M["m00"]))
    cv2.circle(img, center, int(radius), (0, 0, 255), 4)


    print(center)
    
    cv2.imshow("img", img)
    cv2.waitKey(0)
    pass
else:
    print("nothing detected")
    pass
