from pyparrot.Bebop import Bebop
from pyparrot.DroneVisionGUI import DroneVisionGUI
import threading
import cv2
import time
from time import sleep
from PyQt5.QtGui import QImage

from enum import Enum, auto

class Model(Enum):
    BEBOP = auto()
	
    
isAlive = False
filename = ""

class UserVision:
    def __init__(self, vision):
        self.vision = vision
        self.index = 0

    def save_pictures(self, args):
        img = self.vision.get_latest_valid_picture()

        # limiting the pictures to the first 10 just to limit the demo from writing out a ton of files
        if (img is not None and self.index <= 100000):
            global filename
            filename = "imgs/test_image_%06d.png" % self.index
            print(filename)
            cv2.imwrite(filename, img)
            self.index +=1
            #sleep(0.1)

def draw_current_photo():
    print(filename)
    image = cv2.imread(filename)
    if (image is not None):
        if len(image.shape) < 3 or image.shape[2] == 1:
            image = cv2.cvtColor(image, cv2.COLOR_GRAY2RGB)
        else:
            image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

        height, width, byteValue = image.shape
        byteValue = byteValue * width

        qimage = QImage(image, width, height, byteValue, QImage.Format_RGB888)

        return qimage
    else:
        return None

def demo_user_code_after_vision_opened(bebopVision, args):
    bebop = args[0]

    print("Vision successfully started!")
    
    #bebop.pan_tilt_camera_velocity(pan_velocity=0, tilt_velocity=0, duration=4)
    bebop.pan_tilt_camera(tilt_degrees=-90, pan_degrees=0)

    #bebop.pan_tilt_camera_velocity(pan_velocity=0, tilt_velocity=90, duration=1)
    bebop.smart_sleep(1)
    bebop.safe_takeoff(2)
    bebop.smart_sleep(1)
    #bebop.move_relative(dx=1, dy=0, dz=0, dradians=0)
    #bebop.move_relative(dx=-1, dy=0, dz=0, dradians=0)
    bebop.move_relative(dx=0, dy=-1, dz=0, dradians=0)
    #bebop.move_relative(dx=0, dy=1, dz=0, dradians=0) 
    bebop.fly_direct(roll=0, pitch=0, yaw=0, vertical_movement=20, duration=2)
    
    for x in range(10):
        bebop.smart_sleep(1)
        bebop.fly_direct(roll=0, pitch=0, yaw=20, vertical_movement=0, duration=2)
        #bebop.fly_direct(roll=0, pitch=10, yaw=0, vertical_movement=0, duration=0.2)
    
    #bebop.fly_direct(roll=0, pitch=0, yaw=75, vertical_movement=0, duration=20)
    
    #bebop.fly_direct(roll=0, pitch=40, yaw=0, vertical_movement=0, duration=3)
    #bebop.fly_direct(roll=0, pitch=-40, yaw=0, vertical_movement=0, duration=2)
    #bebop.fly_direct(roll=0, pitch=-40, yaw=0, vertical_movement=0, duration=1)
    #bebop.fly_direct(roll=0, pitch=40, yaw=0, vertical_movement=0, duration=1)
    bebop.smart_sleep(10)
    #bebop.fly_direct(roll=40, pitch=0, yaw=0, vertical_movement=0, duration=3)
    #bebop.fly_direct(roll=-40, pitch=0, yaw=0, vertical_movement=0, duration=6)
    #bebop.smart_sleep(2)
    #bebop.fly_direct(roll=0, pitch=0, yaw=0, vertical_movement=-30, duration=2)
     
    #bebop.fly_direct(roll=0, pitch=0, yaw=90, vertical_movement=0, duration=4)
    #bebop.move_relative(dx=0, dy=0, dz=0.5, dradians=0)
    #bebop.move_relative(dx=0, dy=0, dz=-1, dradians=0)
    #bebop.move_relative(dx=0, dy=0, dz=1, dradians=0)
    
    bebop.smart_sleep(1)
    bebop.safe_land(5)
    if (bebopVision.vision_running):
        bebop.smart_sleep(5)
        bebop.safe_land(5)
        bebopVision.close_video()
    
    bebopVision.close_video()
    bebop.smart_sleep(20)
    bebop.disconnect()

if __name__ == "__main__":
    bebop = Bebop()
    success = bebop.__init__("Bebop2","192.168.42.1")
    success = bebop.connect(5)

    if (success):
        bebopVision = DroneVisionGUI(bebop, Model.BEBOP, user_code_to_run=demo_user_code_after_vision_opened, user_args=(bebop, ), user_draw_window_fn=draw_current_photo)
        userVision = UserVision(bebopVision)
        bebopVision.set_user_callback_function(userVision.save_pictures, user_callback_args=None)
        bebopVision.open_video()

    else:
        print("Error connecting to bebop.  Retry")

