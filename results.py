import numpy as np
import matplotlib.pyplot as plt
from enum import Enum 

class Configs(Enum):
    low = 'low'
    mid = 'mid'
    high = 'high'


def loadqtable(id, config):
    return np.loadtxt(f'data/{config.value}/qtable{id}.txt')
def loadheatMap(id, config): 
    return np.loadtxt(f'data/{config.value}/heatmap{id}.txt')
def loadrewards(id, config):
    return np.loadtxt(f'data/{config.value}/rewards{id}.txt')

def loadrewards(id, config):
    x = np.loadtxt(f'data/{config.value}/rewards{id}.txt')
    # r = []
    # i = 0
    # sub = []
    # for q in x:
    #     if i % 10 == 0:
    #         r.append(np.average(sub))
    #         sub.clear()
    #     else:
    #         sub.append(q)
    #     i+=1
    # return r
    return x

def plotReward(id, config):
    plt.cla()
    plt.plot(loadrewards(id, config))
    plt.xlabel('Episode')
    plt.ylabel('Reward')
    plt.draw()
    plt.show(block=True)

def plotHeatmap(id, config, isBlocking=True, savepath=None):
    plt.cla()
    plt.imshow(loadheatMap(id, config), aspect = 0.07)
    plt.xlabel('Action')
    plt.ylabel('State')
    plt.draw()
    if savepath != None:
        plt.savefig(savepath)
    plt.show(block=isBlocking)

# plotReward(11)
# for i in range(14):
    # plotHeatmap(i, False, f'data/_qtable_senarioId{i}.png')

# plotReward(0, Configs.low)
plotHeatmap(0, Configs.low)