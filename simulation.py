import turtle
import time
import random
import math
from enum import IntEnum

class MovementType(IntEnum):
    random = 0
    manuel = 1
    circle = 2
    sin = 3
    infinity = 4
    sinx = 5

class MovingObject:
    def __init__(self, x, y):
        self.prefVelocity = [0, 0]
        self.prevPostion = [x, y]
        self.postion = [x, y]
        self.elapsedTime = 0
        self.time = 0
        self.heading = 0

    worldHeight = 600
    
    def goto(self, x, y):
        limit = self.worldHeight / 2
        if x > limit:
            x = limit
        if x < -limit:
            x = -limit  

        if y > limit:
            y = limit
        if y < -limit:
            y = -limit  

        self.prevPostion = self.postion
        self.postion = [x, y]
        self.elapsedTime = time.time() - self.time
        self.time = time.time()

    def setheading(self, heading):
        self.heading = heading

    def forward(self, speed):
        (x1, y1) = self.postion
        angle = math.radians(self.heading)
        (x2, y2) = (x1 + speed*math.cos(angle),y1 + speed*math.sin(angle))
        self.goto(x2, y2)

    def xcor(self):
        return self.postion[0]
    def ycor(self):
        return self.postion[1]
    def pos(self):
        [x, y] = self.postion
        return (x, y)

    def velocity(self):    
        [x2, y2] = self.postion
        [x1, y1] = self.prevPostion
        vx = (x2 - x1) / (self.elapsedTime if self.elapsedTime != 0 else 1)
        vy = (y2 - y1) / (self.elapsedTime if self.elapsedTime != 0 else 1)
        return round(vx), round(vy)

    def acceleration(self):
        [vx2, vy2] = self.velocity()
        [vx1, vy1] = self.prefVelocity
        a1 = (vx2 - vx1) / (self.elapsedTime if self.elapsedTime != 0 else 1)
        a2 = (vy2 - vy1) / (self.elapsedTime if self.elapsedTime != 0 else 1)
        return round(a1), round(a2)


class FakeObjectBuilder(MovingObject):
    def __init__(self, initPosition=None, type=None, worldHeight=None, changeMovementType=None,delay=None):
        if initPosition == None:
            initPosition = (0, 0)
        super().__init__(*initPosition)
        pass
    def clear(self):
        pass
    def penup(self):
        pass
    def pendown(self):
        pass
    def write(self, txt):
        pass
    def update(self):
        pass
    def onkey(func, key):
        pass

    
hideAnimation = False
class RealObjectBuilder(MovingObject):
    def __init__(self, initPosition=None, type=None, worldHeight=None, changeMovementType=None,delay=None):
        if initPosition == None:
            initPosition = (0, 0)
        super().__init__(*initPosition)
        self.initPosition = initPosition
        
        if type == 'drone':
            self.obj = turtle.Turtle()
            self.obj.shape("square")
            self.obj.color("black")
            self.obj.pencolor(0.8,0.8,0.8)
            self.obj.pensize(2)
            self.obj.ondrag(self.goto)
        elif type == 'target':
            self.obj = turtle.Turtle()
            self.obj.shape("circle")
            self.obj.shapesize(2)
            self.obj.color("red")
            self.obj.pencolor(1, 0.75, 0.75)
            self.obj.ondrag(self.goto)
        elif type == 'text':
            self.obj = turtle.Turtle()
            self.obj.hideturtle()
            self.obj.pencolor('red')
            self.obj.penup()
        elif type == 'line':
            self.obj = turtle.Turtle()
            self.obj.shape('circle')
            self.obj.color('orange')
            self.obj.shapesize(0.4,0.4,0.4)
            self.obj.penup()
            self.obj.pensize(3)
        elif type == 'wn':
            self.obj = turtle.Screen()
            self.obj.title("drone simulation")
            self.obj.setup(width=worldHeight, height=worldHeight)
            self.obj.onkey(self.toggleHideAnimation,'h')
            self.obj.onkey(changeMovementType,'x')
            self.obj.listen()
            self.obj.tracer(0)
            self.delay = delay
    
    def onkey(self, func, key):
        self.obj.onkey(func, key)
    
    def toggleHideAnimation(self):
        global hideAnimation
        hideAnimation = not hideAnimation

    obj = None
    delay = None
    def goto(self, x, y):
        super().goto(x, y)
        self.obj.goto((x, y))

    def clear(self):
        self.obj.clear()

    def penup(self):
        self.obj.penup()

    def pendown(self):
        self.obj.pendown()

    counter = 0
    def write(self, txt):
        self.counter += 1
        if hideAnimation == True and self.counter % 1000 != 0:
            return
        self.obj.clear()
        self.obj.goto(*self.initPosition)
        self.obj.write(txt, font=("Arial", 18, "normal"))

    def update(self):
        if hideAnimation == True:
            return
        if self.delay != None and self.delay > 0:
            time.sleep(self.delay / 1000)
        self.obj.update()
    
class Env:
    def __init__(self, ObjectBuilder,
     targetMovementType = MovementType.infinity,
     droneInitPosition = (100, -100),
     delay = 1):
        self.targetspeed = 2
        self.targetMovementType = targetMovementType 

        self.droneInitPosition = droneInitPosition
        self.targetInitPosition = (0, 0)
        self.textInitPosition = (-280, 200)

        self.worldHeight = 600

        self.wn = ObjectBuilder(type='wn', worldHeight=self.worldHeight, changeMovementType=self.changeMovementType, delay=delay)
        self.target = ObjectBuilder(initPosition=self.targetInitPosition, type='target')
        self.drone = ObjectBuilder(initPosition=self.droneInitPosition, type='drone')
        self.text = ObjectBuilder(initPosition=self.textInitPosition, type='text')
        self.line = ObjectBuilder(initPosition=self.droneInitPosition, type='line')
        self.reset()

    def reset(self):
        self.drone.clear()
        self.target.clear()
        self.target.penup()
        self.drone.penup()
        self.target.goto(*self.targetInitPosition)
        dronPosition = self.droneInitPosition
        if self.droneInitPosition == None:
            dronPosition = (random.randint(-1 * self.worldHeight / 2, self.worldHeight /2),
            random.randint(-1 * self.worldHeight / 2, self.worldHeight /2))
        self.drone.goto(*dronPosition)
        self.target.pendown()
        self.drone.pendown()
        
        self.theta = 0
        self.prevRandX = 9999
        self.prevRandY = 9999
        self.counterSin = 0
        self.counterX = 0
        self.droneXDirection = None
        self.droneYDirection = None

    def write(self, txt):
        self.text.write(txt)
    
    def targetMovementGenerator(self):
        (x, y) = self.target.pos()
        type = self.targetMovementType
        height = (self.worldHeight / 2) - 10

        if self.targetspeed == 0 or type == MovementType.manuel:
            return x, y
        if type == MovementType.circle:
            self.theta += self.targetspeed* 10 / 600
            r = self.radius
            targetx = r * math.cos(self.theta)
            targety = r * math.sin(self.theta)
        elif type == MovementType.random:
            if (abs(x - self.prevRandX) <= self.targetspeed and abs(y - self.prevRandY) <= self.targetspeed) or self.prevRandX == 9999:
                self.prevRandX = random.randint(-height, height)
                self.prevRandY = random.randint(-height, height)
            targetx, targety = self.mover(x , y, self.prevRandX, self.prevRandY, self.targetspeed)
        elif type == MovementType.sin:
            targetx = (math.sin(self.counterSin) * height)
            targety = (math.sin(self.counterSin) * height)
            self.counterSin += (math.pi / (height * (1 / self.targetspeed * 2)))
        elif type == MovementType.sinx:
            targetx = (math.sin(self.counterSin) * height)
            targety = y
            self.counterSin += (math.pi / (height * (1 / self.targetspeed * 2)))
        elif type == MovementType.infinity:
            targetx = (math.sin(self.counterSin) * height)
            targety = math.sin(self.counterX) * (height / 2)
            self.counterX += (math.pi / ((height / 2) * (1 / self.targetspeed * 2.5)))
            self.counterSin += (math.pi / (height * (1 / self.targetspeed * 2.5)))
        else:
            return x, y
        return targetx, targety

    def movetarget(self):
        self.target.goto(*self.targetMovementGenerator())

    def addStep(self, v, delta, steps, speed):
        if speed == 0 or steps == 0 or (steps/ speed) == 0:
            return v
        return v + (delta / (steps/ speed))

    def mover(self, x1, y1, x2, y2, speed):
        dx = abs(x2 - x1)
        dy = abs(y2 - y1)
        steps = max(dx, dy)
        return self.addStep(x1, x2 - x1, steps, speed), self.addStep(y1, y2 - y1, steps, speed)

    # def getSpeed(self, newSpeed):
    #     if abs(newSpeed) <= self.droneMaxSpeed:
    #         return newSpeed
    #     return self.droneMaxSpeed if newSpeed > 0 else -self.droneMaxSpeed

    def update(self):
        self.wn.update()
        self.movetarget()
    
    def command(self, heading, speed):
        self.drone.setheading(heading)
        self.drone.forward(speed)
        self.update()

    def movedrone(self, x, y, speed):
        (dx, dy) = self.drone.pos()
        x, y = self.mover(x, y, dx, dy, speed)
        self.drone.goto(x, y)
        self.update()

    def changeMovementType(self, targetMovementType = None, Radius = None, speed = 2):
        if targetMovementType == None:
            targetMovementType = random.choice(list(MovementType))
        self.radius = Radius
        self.targetspeed = speed
        self.targetMovementType = targetMovementType

    def state(self):
        dronePos = self.drone.pos()
        droneVelocity =  self.drone.velocity()
        droneAcceleration = self.drone.acceleration()
        targetPos = self.target.pos()
        targetVelocity = self.target.velocity()
        targetAcceleration = self.target.acceleration()
        return dronePos, droneVelocity, droneAcceleration, targetPos, targetVelocity, targetAcceleration
    
    headingForSimulation = 0
    def up(self):
        self.headingForSimulation = 90
    def down(self):
            self.headingForSimulation = 270
    def left(self):
            self.headingForSimulation = 180
    def right(self):
            self.headingForSimulation = 0

    def simulate(self):
        self.write('use [w, s, a, d] to control the drone')
        self.wn.onkey(self.up ,'w')
        self.wn.onkey(self.down ,'s')
        self.wn.onkey(self.left ,'a')
        self.wn.onkey(self.right ,'d')
        while True: 
            self.drone.setheading(self.headingForSimulation)
            self.drone.forward(2)
            self.update()
            # self.followTarget()


    def droneVelocity(self):
        return self.drone.velocity()
    def droneAcceleration(self):
        return self.drone.acceleration()

    def targetVelocity(self):
        return self.target.velocity()
    def targetAcceleration(self):
        return self.target.acceleration()

# Env(RealObjectBuilder).simulate()