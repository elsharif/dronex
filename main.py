from trainer import Trainer, Configs

trainer = Trainer()

trainer.playPID(senarioId=1, p=0.2, i=0.9)
trainer.playQL(Configs.high, senarioId=1)


exit(1)

trainer.trainSenario(Configs.high, epsilon=0, alpha=0.2,gamma=0.9,senarioId=4)
for senarioId in range(len(trainer.senarios)):
    trainer.plotall(senarioId)

print('start to training the drone (low config)')
trainer.trainAllSenarios(Configs.low, epsilon=0.8, alpha=0.2,gamma=0.9,senarioId=senarioId)

print('start to training the drone (Mid config)')
trainer.trainSenario(Configs.mid, epsilon=0.8, alpha=0.2,gamma=0.9,senarioId=senarioId)

print('start to training the drone (high config)')
trainer.trainSenario(Configs.high, epsilon=0.8, alpha=0.2,gamma=0.9,senarioId=senarioId)
