
import math
import random
import time
import numpy as np
from enum import Enum
import matplotlib.pyplot as plt
from simple_pid import PID
from simulation import Env, RealObjectBuilder, FakeObjectBuilder, MovementType


class Configs(Enum):
    low = 'low'
    mid = 'mid'
    high = 'high'

    
def name(enu):
    return str(enu).split('.')[1]

def appandText(path, values):
    file_object = open(path, 'a')
    for v in values:
        file_object.write(str(v) + '\n')
    file_object.close()

class Trainer:
    senarios = [
        (0, MovementType.manuel, 0.5, 100),
        (1, MovementType.random, 2, 200),
        (2, MovementType.random, 4, 200),
        (3, MovementType.random, 6, 200),
        (4, MovementType.infinity, 1, 200),
        (5, MovementType.infinity, 4, 200),
        (6, MovementType.infinity, 6, 200),
        (7, MovementType.circle, 0.5, 200),
        (8, MovementType.circle, 1, 200),
        (9, MovementType.circle, 3, 200),
        (10, MovementType.circle, 0.5, 100),
        (11, MovementType.circle, 1, 100),
        (12, MovementType.circle, 3, 100),
        ]

    def __init__(self):
        self.maxSpeed = 10

    def changeConfig(self, config):
        if config == Configs.high:
            self.StateCount = 40
            self.ActionHeadingCount = 32
            self.ActionSpeedCount = 10
            self.dataFolderPrefex = 'high'
        elif config == Configs.mid:
            self.StateCount = 20
            self.ActionHeadingCount = 16
            self.ActionSpeedCount = 6
            self.dataFolderPrefex = 'mid'
        elif config == Configs.low:
            self.StateCount = 10
            self.ActionHeadingCount = 4   
            self.ActionSpeedCount = 4
            self.dataFolderPrefex = 'low'
        self._initialzeForActionAndStateVariable()

    def _initialzeForActionAndStateVariable(self):
        self.Heading = []
        for h in range(self.ActionHeadingCount):
            self.Heading.append(round(360 * h / self.ActionHeadingCount))
        self.Speed = []
        for h in range(self.ActionSpeedCount):
            self.Speed.append(round((self.maxSpeed * h) / self.ActionSpeedCount))
        self.statesize = (self.StateCount * 2)**2 + (self.StateCount * 2)
        self.actionsize = len(self.Heading) * len(self.Speed)

    def loadDataFile(self, path):
        if np.DataSource().exists(path):
            return np.loadtxt(path)
        self._initialzeForActionAndStateVariable()
        return np.zeros((self.statesize, self.actionsize))


    def getActionIndex(self, action):
        return (self.Heading.index(action[0]) * len(self.Speed)) + (self.Speed.index(action[1]))

    def get2dActionFromIndex(self, actionIndex):
        i = int(actionIndex / len(self.Speed))
        j = int(actionIndex % len(self.Speed))
        return self.Heading[i], self.Speed[j]

    def getQuantifiedState(self, state):
        [dx, dy], [_, _], [_, _], [tx, ty], [_, _], [_, _] = state
        
        x, y = abs(tx - dx), abs(ty - dy)
        xdirection = +1 if tx >= dx else -1
        ydirection = +1 if ty >= dy else -1 

        xIndex = round(x / (self.env.worldHeight / self.StateCount))
        yIndex = round(y / (self.env.worldHeight / self.StateCount))
        return xIndex * xdirection, yIndex * ydirection


    def getR(self, x1, y1, x2, y2):
        return ((x1 - x2)**2 + (y1 - y2)**2)**0.5

    rewardPrev = 0
    def getReward(self, state):
        [dx, dy], [dVx, dVy], [dAx, dAy], [tx, ty], [tVx, tVy], [tAx, tAy] = state
        r = self.getR(tx, ty, dx, dy)

        # if r <= T1:
        #     r = C1 * r
        #     r += C2 * getR(tVx, tVy, dVx, dVy)
        #     # r += C3 * getR(tAx, tAy, dAx, dAy)
        
        reward = r - self.rewardPrev
        self.rewardPrev = r
        return -reward

    def isTerminationState(self, pysicalState):
        [dx, dy], [_, _], [_, _], [tx, ty], [_, _], [_, _],  = pysicalState
        r = self.getR(tx, ty, dx, dy)
        if r <= 14:
            return True
        return False

    def getStateIndex(self, state):
        x = state[0] + self.StateCount
        y = state[1] + self.StateCount
        return y + (x * self.StateCount * 2) 

    def selectAction(self, epsilon, Q, state):
        if self.random.uniform(0, 1) < epsilon:
            return self.explore()
        return self.exploit(Q, state)
            
    def explore(self):
        return self.random.choice(self.Heading), self.random.choice(self.Speed)

    def exploit(self, Q, state):
        return self.get2dActionFromIndex(np.argmax(Q[self.getStateIndex(state), :]))

    def updateQTable(Q, alpha, gamma, action, state, new_state, reward, heatmap):
        state = self.getStateIndex(state)
        new_state = self.getStateIndex(new_state)
        action = self.getActionIndex(action)
        maxNextStateReward = np.max(Q[new_state,:])
        Q[state, action] = ((1 - alpha) * Q[state, action]) + (alpha * (reward + gamma * maxNextStateReward))
        heatmap[state, action] += 1

    def trainSenario(self, config, epsilon, alpha, gamma, senarioId, hideAnimation=False):
        (number, moveType, targetSpeed, radius) = self.senarios[senarioId]
        qtpath = f'data/{self.dataFolderPrefex}/qtable{number}.txt'
        hmpath = f'data/{self.dataFolderPrefex}/heatmap{number}.txt'
        rpath = f'data/{self.dataFolderPrefex}/rewards{number}.txt'
        return self.train(config=config, epsilon=epsilon, alpha=alpha,gamma=gamma, movementType=moveType, targetspeed=targetSpeed,Radius=radius, qtablePath=qtpath, heatmapPath=hmpath, rewardsPath=rpath,hideAnimation=hideAnimation)

    def trainAllSenarios(self,config, epsilon, alpha, gamma, senarioId, hideAnimation=False):
        for senarioId in range(len(self.senarios)):
            print(f'senario {senarioId}')
            for _ in range(10):
                self.trainSenario(config=config, epsilon=epsilon, alpha=alpha,gamma=gamma,senarioId=senarioId, hideAnimation=hideAnimation)

    def train(self, config, epsilon, alpha, gamma, movementType, targetspeed, Radius, qtablePath, heatmapPath, rewardsPath,hideAnimation=True):        
        start_time = time.time()

        self.changeConfig(config)
        
        self.env = Env(RealObjectBuilder if hideAnimation==False else FakeObjectBuilder, None, None)
        Q = loadDataFile(qtablePath)
        heatmap = loadDataFile(heatmapPath)
        self.env.changeMovementType(movementType, Radius, targetspeed)
        episodeCount = 1000

        self.rewards = []
        for e in range(episodeCount):
            self.env.reset()
            episodeReward = 0
            newState = (0,0)
            for _ in range(10_000):
                oldState = newState
                action = self.selectAction(epsilon, Q, oldState)
                self.env.command(*action)
                pysicalState = self.env.state()
                newState = self.getQuantifiedState(pysicalState)
                reward = self.getReward(pysicalState)
                episodeReward += reward
                if epsilon != 0:
                    self.updateQTable(Q, alpha, gamma, action, oldState, newState, reward, heatmap)
                if self.isTerminationState(pysicalState):
                    break
                if hideAnimation == False:
                    self.env.write(f'e={e}\naction={action}\nstate={newState}\nr={reward}')

            self.rewards.append(episodeReward)
        np.savetxt(qtablePath, Q)
        np.savetxt(heatmapPath, heatmap)
        appandText(rewardsPath, self.rewards)
        self.episodeCountTotal += episodeCount
        print(f'e= {episodeCountTotal} avgR= {round(np.average(rewards), 6)} time= {round(time.time() - start_time)} sec')

    def playQL(self, config, senarioId, hideAnimation= False):
        self.changeConfig(config)
        (number, moveType, targetSpeed, radius) = self.senarios[senarioId]    
        qtablePath=f'data/{self.dataFolderPrefex}/qtable{number}.txt'
        Q = self.loadDataFile(qtablePath)
        self.env = Env(RealObjectBuilder if hideAnimation == False else FakeObjectBuilder, None, None)
        self.env.changeMovementType(moveType, radius, targetSpeed)
        self.resetError()
        for step in range(1000):
            state = self.getQuantifiedState(self.env.state())
            action = self.exploit(Q, state)
            self.env.command(*action)
            self.addError()
            self.env.write(f'step={step}\nstate={state}\naction={action}')
        return self.values
        

    def playPID(self, senarioId, p=0, i=0, d=0):    
        (senarioNumber, moveType, targetSpeed, radius) = self.senarios[senarioId]    

        self.env = Env(RealObjectBuilder, None, None)
        self.env.reset()
        self.env.update()
        self.env.changeMovementType(moveType, radius, targetSpeed / 2)

        P = p
        I = i
        D = d

        pidX = PID(P,I,D, sample_time=0.01)
        pidY = PID(P,I,D, sample_time=0.01)
        self.resetError()
        for step in range(1000):
            [dx, dy], [_, _], [_, _], [tx, ty], [_, _], [_,_] = self.env.state()
            pidX.setpoint = tx
            pidY.setpoint = ty
            controlX = pidX(dx)
            controlY = pidY(dy)
            self.env.movedrone(controlX, controlY, self.maxSpeed)
            self.addError()
            self.env.write(f'step={step}')
        # np.savetxt(cacheFilePath, values)
        return self.values


    def resetError(self):
        self.values = []
        self.env.reset()
        self.env.update()
    
    def addError(self):
        [dx, dy], [_, _], [_, _], [tx, ty], [_, _], [_,_] = self.env.state()
        self.values.append(((tx - dx)**2 +  (ty - dy)**2)**0.5)

    def plot(self, values, axies, savepath, Block= True):
        plt.cla()
        for v in values:
            plt.plot(v)
        plt.legend(axies)
        plt.ylabel('Error')
        plt.xlabel('Time')

        plt.savefig(savepath)
        plt.draw()
        plt.show(block=Block)

    def plotall(self, s):
        v1 = self.playPID(senarioId=s, p=0.2)
        v2 = self.playPID(senarioId=s, p=0.2, i=0.9)
        v3 = self.playPID(senarioId=s, p=0.2, d=0.01)
        v4 = self.playPID(senarioId=s, p=0.2, i=0.9, d=0.01)
        self.setLowConfig() 
        v5 = self.playQL(senarioId=s, hideAnimation=True)
        self.setMidConfig() 
        v6 = self.playQL(senarioId=s, hideAnimation=True)
        self.setHighConfig() 
        v7 = self.playQL(senarioId=s, hideAnimation=True)
        self.plot([v1, v2, v3, v4, v5, v6, v7], ['p','pi','pd','pid','ql low', 'ql mid', 'ql high'], f'data/figures/_error_senario-{s}.png', False)

